##
ns_register_proc GET /ix/::ink inkMake
ns_register_proc POST /ix/::ink inkSave
##

proc inkMake {} {

ns_return 200 text/html \
"<html>
<head>
<title>Ink Message</title>
</head>
<form enctype='multipart/form-data' action='::ink' method='POST'>
Alias: <input type='text' name='alias'><br>
Msg: <input type='text' name='msg'><br>
  <button type='submit' formmethod='post'>:SHOUT</button>
</form>
</body></html>
"}

proc inkSave {} {
set formdata [ns_getform]
set alias "[ns_set get $formdata alias]"
set msg "[ns_set get $formdata msg]"
set myPath "/srv/.forest./.:./teddywarriors.tf/tunnel/::"
set myFuture "http://teddywarriors.tf/::paws2ink"   


  ##
set fp [open "$myPath/$alias" a]
puts $fp "$alias :*: $msg\n"
close $fp
  ##
  
puts "$alias"

ns_return 200 text/html \
"<html>
<body>
poke ~ $alias : $msg
</body></html>"
}
